# Maven Environment

This project is part of the OpenTestFactory initiative.

It generates a Docker image usable for executing tests driven by Maven (i.e. Cucumber and JUnit tests).

The [OpenTestFactory documentation](https://opentestfactory.org/extra/maven-environment-image.html) describes how to use this image.